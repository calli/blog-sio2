<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;


class ArticleController extends AbstractController
{
    #[IsGranted('ROLE_USER')]
    #[Route('/addarticle', name: 'add_article')]
    public function add(Request $request, EntityManagerInterface $em): Response
    {
        $article = new Article();
        $article->setAuteur($this->getUser());
        $article->setDateCreation(new \DateTime());

        $form = $this->createForm(ArticleType::class , $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();
            // on sauvegarde en bdd
            $em->persist($article);
            $em->flush(); // Execute en bdd les requetes sql (insert, update)
            return $this->redirectToRoute('app_home');
        }
        return $this->render('article/add.html.twig', [
            'form' => $form
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/editarticle/{id}', name: 'edit_article')]
    public function edit(Article $article, Request $request, EntityManagerInterface $em): Response
    {

        $form = $this->createForm(ArticleType::class , $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();
            // on sauvegarde en bdd
            $em->persist($article);
            $em->flush(); // Execute en bdd les requetes sql (insert, update)
            return $this->redirectToRoute('app_home');
        }
        return $this->render('article/edit.html.twig', [
            'form' => $form
        ]);
    }
}
